<?php 
// Load & Parse JSON
$json = file_get_contents('https://raw.githubusercontent.com/jakeyarrington/Code-Test/master/people.json');
$people = json_decode($json);

// Modify array values
foreach ($people as $obj) {
    $obj->age += 5;
    echo $obj->age;
 }
?>
