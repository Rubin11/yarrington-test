<?php 
function people_function() {
	// Load & Parse JSON
    $json = file_get_contents('https://raw.githubusercontent.com/jakeyarrington/Code-Test/master/people.json');
    $people = json_decode($json);

    // Modify array values
    $html = '<div class="people-list">';

    // Loop through each person & output data into HTML tags
    foreach ($people as $obj) {
        $obj->age += 5;
        $html .= '<div class="person">
        <div class="person--image">
            <img alt="" src="https://via.placeholder.com/70.png">
            </div>
        <div class="person--details">
            <div class="person--name">'.$obj->name.'</div>
            <div class="person--age">Age: '.$obj->age.' </div>
            <div class="person--location">Location: '.$obj->location.', '.$obj->country.'</div>
            <div class="person--email">Email: 
                <span _ngcontent-wjq-c1="">'.$obj->email_address.'</span>
            </div>
        </div>
    </div>';
    }
    $html .= '</div>';

    return $html;
}
// Add shortcode
add_shortcode('people', 'people_function');

// Load css styles
wp_enqueue_style( 'people', get_template_directory_uri() . '/css/people.css');
?>


		