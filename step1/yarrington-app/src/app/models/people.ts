export class People {
    name: string
    age: number
    email_address: string
    location: string
    country: string
}