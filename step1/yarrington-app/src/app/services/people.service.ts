import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { People } from '../models/people';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {
  peopleUrl:string = 'https://raw.githubusercontent.com/jakeyarrington/Code-Test/master/people.json';

  constructor(private http:HttpClient) { }

  getPeople():Observable<People[]> {
    return this.http.get<People[]>(this.peopleUrl)
  }
}
